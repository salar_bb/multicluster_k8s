/*
You have a Custom Resource Definition (CRD) for each resource that you wish to federate with three sections:
the standard resource definition such as a Deployment
a placement section where you can define how that resource should be distributed in the federation
an override section where you can override weights and settings specified in the placement just for a particular cluster
Here's an example of a federated Deployment with placements and overrides. 

As you can imagine, the Deployment is distributed in two clusters: cluster1 and cluster2.
The first cluster deploys three replicas whereas the second overrides the value to 5.
If you wish to have more control on the number of replicas, kubefed2 exposes a new object called ReplicaSchedulingPreference where you can distribute replicas in weighted proportions:
*/